#Koda nagon sorts modul för password, vet inte riktigt hur det ska se ut men typ en password modul man kan flytta runt, krypterad json som lagrar databasen, admin pass som dekrypterar den
# Testa vilken som fungerar: "Do you mean json.dump or json.dumps?"

import json
import simplejson
key = 'abcdefghijklmnopqrstuvwxyz'

def readJSON():
	json_file='auth.json'
	json_data=open(json_file)
	data = simplejson.load(json_data)
	return data
	


def encrypt(n, plaintext):
	result = ''
	for l in plaintext.lower():
		try:
			i = (key.index(l) + n) % 26
			result += key[i]
		except ValueError:
			result += l
	return result.lower()
	
def decrypt(n, ciphertext):
	result = ''
	for l in ciphertext:
		try:
			i = (key.index(l) - n) % 26
			result += key[i]
		except ValueError:
			result += l
	return result
	
def writeToFile(jsoncipher):
	target = open('encryptedjson', 'w')
	#target.write(jsoncipher)
	#json.dump(jsoncipher, target)
	simplejson.dump(jsoncipher, target)
	target.close()
	
def readEncrypted():
	target = open('encryptedjson', 'r')
	line = target.read()
	target.close()
	return line
	
def create():
	jsonstring = readJSON()
	input = json.dumps(jsonstring)
	encrypted = encrypt(len(input), input)
	encryptedstring = simplejson.loads(encrypted)
	writeToFile(encryptedstring)
	
	
def show_result(plaintext, n):
	encrypted = encrypt(n, plaintext)
	decrypted = decrypt(n, encrypted)
	print(encrypted)
	print(decrypted)
	

	
class User(object):
		def __init__(self, user):
			self.user = user
	
def printstuff():	
	line = readEncrypted()
	decrypted = decrypt(len(line), line)
	data = simplejson.loads(decrypted)
	u = User(**data)
	print(u.user)

	
	
#create()
printstuff()





























