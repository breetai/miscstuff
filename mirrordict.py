
# Dubbel dict med mirrorfunktion
class MirrorDict(dict):
	mirror = {}
	def __init__(self, *args, **kw):
		super(MirrorDict,self).__init__(*args, **kw)
		self.itemlist = super(MirrorDict,self).keys()
		self.mirror = {v: k for k, v in self.items()}
		
		
	
asd = MirrorDict(a='2',b='3')
print(asd)
print(asd.mirror)