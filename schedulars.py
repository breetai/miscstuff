from random import randint
import time

class Process:
	time = 0
	name = ""
	def __init__(self, time, name):
		self.time = time
		self.name = name
	def execute(self):
		print("executing " + self.name)
	def __str__(self):
		return "name: "+self.name+", time = "+str(self.time)
	def __repr__(self):
		return "name: "+self.name+", time = "+str(self.time)

processList = []


def createProcesses(numberOfProcesses):
	for i in range(0, numberOfProcesses):
		t = randint(1,30)
		p = Process(t, str(i))
		processList.append(p)
	
	
def contextSwitch():
	print("context switch")
	
def shortestJobFirst():
	# To sort the list in place...
	#processList.sort(key=lambda x: x.time, reverse=True)
	# To return a new list, use the sorted() built-in function...
	#newlist = sorted(processList, key=lambda x: x.time, reverse=True)
	newlist = sorted(processList, key=lambda x: x.time, reverse=False)
	for p in newlist:
		p.execute()
		time.sleep(p.time)
		
def firstInFirstOut():
	for i in processList:
		p.execute()
		time.sleep(p.time)
	
	

	
def roundRobin():
	processingTime = 10
	loop = True
	while(loop == True):
		print("Looping")
		for process in processList:
			if(process.time > 0):
				process.execute()
				time.sleep(processingTime)
				process.time = process.time - processingTime
				#if(process.time < 0):
					#print("reomving" + process.name)
					#processList.remove(process) creates list bug when iterating
		if(len(processList) < 1):
			loop = False
	
def roundRobinWhile():
	processingTime = 10
	loop = True
	i = 0
	processListLength = 0
	while(loop == True):
	#while(len(processList) > 0):
		#processListLength = len(processList)
		processList[i].execute()
		time.sleep(processingTime)
		processList[i].time = processList[i].time - processingTime
		if(processList[i].time < 0):
			print("reomving" + processList[i].name)
			processList.remove(processList[i])
			i = i - 1
		#i = i +  (i % len(processList))
		print("i = " +str(i) + " and len = " + str(len(processList)))
		if(len(processList) < 1):
			loop = False
		else:
			i = (i + 1) % len(processList)


	
print("hello")
createProcesses(5)
print(processList)
roundRobinWhile()
#shortestJobFirst()

