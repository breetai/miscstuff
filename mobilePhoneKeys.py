# author: klp
# Converts between phone number and letters on every key on the number pad
def getChars(keyArray, translation):
	output = ""
	for num in keyArray:
		output = output + translation[num]
	return output

def readWords(str):
	output = []
	i = 0
	iterations = len(str)
	while(i < iterations):
		if(str[0] == "\n" or str[0] == " "):
			size = 1
		elif(str[0] == 'p' or str[0] == 'w'):
			size = 4
		else:
			size = 3
		output.append(str[:size])
		str = str[size:]
		i = i + size
	return output

#getChars("pqrsdeftuvghipqrsmnodefmnomno")

"pqrsdef ghipqrsmnodefmnomno"

translation = {'1' : ' ' ,'2': 'abc', '3': 'def', '4': 'ghi','5': 'jkl','6': 'mno','7': 'pqrs','8': 'tuv','9': 'wxyz', '0' : '\n'}
takeChars = dict((v, k) for k, v in translation.items())

op = readWords("\npqrsdefmno tuvjkldefabcmno")
numbers=list("0736185326")

print(getChars(numbers, translation))
print(getChars(op, takeChars))

print(getChars(readWords("\npqrsdefmno tuvjkldefabcmno"),takeChars))

#if(op == ["\n", "pqrs","def","tuv","ghi","pqrs","mno","def","mno","mno"]):
	#print("pass")
